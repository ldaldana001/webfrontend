import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LoginView from "@/views/LoginView.vue";
import SignUpView from "@/views/SignUpView.vue";
import ProfileView from "@/views/ProfileView.vue";
import WishListView from "@/views/WishListView.vue";
import ProfileParent from "@/views/ProfileParent.vue";
import ItemsView from "@/views/ItemsView.vue";
import ReservedItemsView from "@/views/ReservedItemsView.vue";
import starterHomeView from "@/views/starterHomeView.vue";
import chatsView from "@/views/chatsView.vue";
import SearchUserView from "@/views/SearchUserView.vue";
import FriendsView from "@/views/FriendsView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      props: true,
      children:[
        {
          path: '/',
          name: 'starterHome',
          component: starterHomeView,
          props: true
        },
        {
          path: '/chat/:userId',
          name: 'chat',
          component: chatsView,
          props: true
        },
        {
          path: '/search/:toSearch',
          name: "search",
          component: SearchUserView,
          props: true
        },
        {
          path: 'user',
          name: 'profileParent',
          component: ProfileParent,
          props: true,
          children:[
            {
              path:':userId',
              name:'profileView',
              component:ProfileView,
              props: true
            },
            {
              path: ':userId/friends',
              name: 'friendsList',
              component: FriendsView,
              props: true
            },
          ]
        },
        {
          path: '/reserved-items',
          name: 'reservedItemsView',
          component: ReservedItemsView,
          props: true
        },
        {
          path:'/wishlist/:wishlistId',
          name:'wishlistView',
          component:WishListView,
          props: true,
        },
        {
          path:':wishlistId/items',
          name:'itemsView',
          component:ItemsView,
          props: true
        },
      ]
    },
    {
      path:'/login',
      name: 'login',
      component: LoginView,
      props: true
    },
    {
      path: '/signup',
      name: 'signup',
      component: SignUpView,
      props: true
    },
  ]
})

export default router
