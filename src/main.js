import { createApp } from 'vue'
import App from './App.vue'
import router from './router'

import './assets/main.css'

const app = createApp(App)
router.afterEach(() => {
    const shouldReload = localStorage.getItem('shouldReload')

    if (shouldReload) {
        localStorage.removeItem('shouldReload')
        window.location.reload()
    } else {
        // Marcar la ruta actual para recargar la próxima vez
        localStorage.setItem('shouldReload', 'true')
    }
})
app.use(router)

app.mount('#app')
